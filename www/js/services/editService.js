angular
    .module('mainApp')
    .factory('EditService', ['localStorageService', function (localStorageService) {
        function getItem() {
            return localStorageService.get('mobile') || {};
        }

        function setItem(val) {
            return localStorageService.set('mobile', val);
        }

        function setEditDetails(val) {
            localStorageService.set('detal', val);
        }

        function getEditDetails() {
           return localStorageService.get('detal') || '';
        }

        return {
            getItem: getItem,
            setItem: setItem,
            setEditDetails: setEditDetails,
            getEditDetails: getEditDetails
        };
    }]);